# Directory Web Service

Directory Web Service is a cross-platform program using a Java 8 (or newer) that that runs in a Docker container, exposes a RESTful interface on port 8080 and allows a client application to obtain the full directory listing of a given path in the container.

## Requirements
The following software needs to be preinstalled on the host machine.
 - [Git](https://www.digitalocean.com/community/tutorials/how-to-install-git-on-debian-10)
 - [Maven 3.8.1](https://linuxize.com/post/how-to-install-apache-maven-on-debian-10/)
 - [Java](https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-on-debian-10) (OpenJDK Runtime Environment (build 15.0.2+7))

## Installation

Use [Git](https://www.digitalocean.com/community/tutorials/how-to-install-git-on-debian-10) to clone the repository.

```bash
git clone https://gitlab.com/3zfr4g/directorywebservice.git
```

Build the docker image using the build script.
```bash
./sh build.sh
```

## Usage
Run the docker image using the run script.
```bash
./sh run.sh -v /path/to/directory -p port
```

There are two exposed endpoints:

 - http://localhost:{port}/healthcheck
   -
Will always return response code 200 while the service is running

- http://localhost:{port}/
   -

Request example:
```
  Method: POST
  URL: http://localhost:{port}/
  Content-Type: application/json
  Body:
    {
	   path :"/", //the path of the requested directory relative to hosted directory
	   pageSize:5, // the amount of results to return
	   page:1 // the requested page number in the overall result set
    }
```
Response example:
```
{
	"currentPath": "/",
	"currentTruePath": "/var/dws",
	"pageSize": 5,
	"currentPage": 6,
	"totalPages": 30002, //The total amount of pages in the current result set for the given page size.
	"files": [{
			"name": "fg10016",
			"path": "/fg10016", //This path should be used in the POST request if it's contents are required. 
			"truePath": "/var/dws/fg10016", //This path is made available only for reference
			"size": 64,
			"isDirectory": true,
			"ownerPermissions": {
				"read": true,
				"write": true,
				"execute": true
			},
			"groupPermissions": {
				"read": true,
				"write": false,
				"execute": true
			},
			"publicPermissions": {
				"read": true,
				"write": false,
				"execute": true
			},
			"owner": "root",
			"group": "root",
			"lastModified": "May 18, 2021, 9:38:18 AM"
		},...
	]
}
```
## Docker Healthcheck
There is a custom healthchecker built into the docker image. It will report the service's health to the Docker environment
```Docker
HEALTHCHECK CMD curl --fail http://localhost:8080/healthcheck || exit 1
```
