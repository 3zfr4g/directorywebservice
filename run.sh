helpFunction()
{
   echo ""
   echo "Usage: $0 -v /path/to.dir -p 9999"
   echo -e "\t-v : path on host which must be traversed"
   echo -e "\t-p : port on host to which the service must be binded"
   exit 1 # Exit script after printing help
}

while getopts "v:p:" opt
do
   case "$opt" in
      v ) parameterV="$OPTARG" ;;
      p ) parameterP="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

# Print helpFunction in case parameters are empty
if [ -z "$parameterV" ] || [ -z "$parameterP" ]
then
   echo "Some or all of the parameters are empty";
   helpFunction
fi

docker run -v $parameterV:/var/dws -p $parameterP:8080 dws:latest &