FROM debian:latest
RUN apt update && apt install -y default-jdk && apt install -y curl
RUN cd ~ && mkdir dws && mkdir /var/dws
COPY ./target/directorywebservice-1.0.0.jar /root/dws
COPY ./target/lib/* /root/dws/lib/
HEALTHCHECK CMD curl --fail http://localhost:8080/healthcheck || exit 1
WORKDIR /root/dws
CMD ["java","-jar","directorywebservice-1.0.0.jar"]