package com.entersekt.helpers;



import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class AppLogger {
    
    static Logger logger;

    public static void info(String message){

        setupLogger();
        logger.info(message);
            
    }

    public static void severe(String message){

        setupLogger();
        logger.severe(message); 
            
    }

    private static void setupLogger() {
        if(logger == null){
            logger = Logger.getLogger("dws.logger");
            logger.setLevel(Level.ALL);
            ConsoleHandler handler = new ConsoleHandler();
            handler.setFormatter(new SimpleFormatter());
            handler.setLevel(Level.ALL);
            logger.addHandler(handler);
        }
    }
}
