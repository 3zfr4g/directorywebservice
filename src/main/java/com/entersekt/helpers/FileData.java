package com.entersekt.helpers;



import java.util.Date;

public class FileData{
    public String name;
    public String path;
    public String truePath;
    public int size;
    public boolean isDirectory;
    public Permissions ownerPermissions = new Permissions();
    public Permissions groupPermissions = new Permissions();
    public Permissions publicPermissions = new Permissions();
    public String owner;
    public String group;
    public Date lastModified;
}
