package com.entersekt.helpers;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileListing {

    //drwxr-xr-x    2 pieter  staff     64 May 18 02:59 fg103
    private static String outputRegex = "^(.+?)\\s+\\d+\\s+(.+?)\\s+(.+?)\\s+(\\d+)\\s+(.+)\\s+(.+)$"; 
    private static String countOutputRegex = "\\s*?(\\d+?)$"; 

    public static ArrayList<FileData> GetDirectoryListing(String root,String directory, int start,int amount) throws Exception{
        
        File froot = new File(root);

        File path = new File(root.concat(froot.separator+directory));
        
        //use --time-style to make sure we get a fixed time format
        String fcmd = String.format("ls -la --time-style='%s' %s | sed -n %d,%dp","+%Y-%m-%d %H:%M:%S",path.getAbsoluteFile(),start,start+amount);
        String[] cmd = { "/bin/sh","-c",fcmd};
        
        Runtime run = Runtime.getRuntime();
        Process pr = run.exec(cmd);
        pr.waitFor();
        BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
        String line = "";
        Pattern pattern = Pattern.compile(outputRegex);

        ArrayList<FileData> files = new ArrayList<FileData>() ;

        while ((line=buf.readLine())!=null) {
            
            Matcher matcher = pattern.matcher(line);
            if(matcher.matches()){

                FileData file = new FileData();
                file.name = matcher.group(6);
                file.path = path.getAbsolutePath().concat(path.separator).concat(file.name).substring(root.length());
                file.truePath = path.getAbsolutePath().concat(path.separator).concat(file.name);
                file.owner = matcher.group(2);
                file.group = matcher.group(3);
                file.size = Integer.parseInt(matcher.group(4));
                
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                file.lastModified = format.parse(matcher.group(5));

                String[] attrib = matcher.group(1).split("");

                if(!attrib[0].equals("-")) file.isDirectory = true;
                if(!attrib[1].equals("-")) file.ownerPermissions.read = true;
                if(!attrib[2].equals("-")) file.ownerPermissions.write = true;
                if(!attrib[3].equals("-")) file.ownerPermissions.execute = true;
                if(!attrib[4].equals("-")) file.groupPermissions.read = true;
                if(!attrib[5].equals("-")) file.groupPermissions.write = true;
                if(!attrib[6].equals("-")) file.groupPermissions.execute = true;
                if(!attrib[7].equals("-")) file.publicPermissions.read = true;
                if(!attrib[8].equals("-")) file.publicPermissions.write = true;
                if(!attrib[9].equals("-")) file.publicPermissions.execute = true;
                
                files.add(file);
            }

        }

        return files;
        
    }


    public static int GetDirectoryCount(File directory) throws Exception{
        
        String fcmd = String.format("ls -la %s | wc -l",directory.getAbsoluteFile());
        String[] cmd = { "/bin/sh","-c",fcmd};
        
        Runtime run = Runtime.getRuntime();
        Process pr = run.exec(cmd);
        pr.waitFor();
        BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
        String line = "";

        Pattern pattern = Pattern.compile(countOutputRegex);
        int count = 0;
        while ((line=buf.readLine())!=null) {
            Matcher matcher = pattern.matcher(line);
            if(matcher.matches()){
                count = Integer.parseInt(matcher.group(1));
            }
        }

        return count;

    }
    
}
