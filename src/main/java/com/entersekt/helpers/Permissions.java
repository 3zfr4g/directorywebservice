package com.entersekt.helpers;

public class Permissions {
    public boolean read = false;
    public boolean write = false;
    public boolean execute = false;
}
