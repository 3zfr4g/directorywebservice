package com.entersekt.handlers;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;

import com.entersekt.helpers.AppLogger;
import com.entersekt.helpers.FileData;
import com.entersekt.helpers.FileListing;
import com.entersekt.requests.DirectoryRequest;
import com.entersekt.responses.DirectoryResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;



public class DirectoryWebServicesHTTPHandler implements HttpHandler{

    String directory;

    public DirectoryWebServicesHTTPHandler(String dir){

        if(dir.endsWith("/")){
            dir = dir.substring(0, dir.length());
        }
        directory = dir;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        
        if(exchange.getRequestURI().toString().equals("/healthcheck")){
            handleHealthRequest(exchange);
            return;
        }

        AppLogger.info(String.format("%h - Request - Method: %s Uri: %s Source: %s",exchange, exchange.getRequestMethod(),exchange.getRequestURI().toString(),exchange.getRemoteAddress().toString()));
        if("POST".equals(exchange.getRequestMethod())){
            handlePostRequest(exchange);
        }else{
            handleErrorReponse(405, "Method Not Allowed", exchange);
        }

    }

    private void handlePostRequest(HttpExchange exchange) throws IOException{
        
        InputStreamReader isReader = new InputStreamReader(exchange.getRequestBody());
        BufferedReader reader = new BufferedReader(isReader);
        StringBuffer sb = new StringBuffer();
        String str;

        while((str = reader.readLine())!= null){
           sb.append(str);
        }
        
        String requestBody = sb.toString();

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
    
        DirectoryRequest request;
        try {
            if(!requestBody.isEmpty()){
                request = gson.fromJson(requestBody, DirectoryRequest.class);
            }else{
                request = new DirectoryRequest();
            }
            
        } catch (JsonSyntaxException e) {
            handleErrorReponse(400, "Bad Request.", exchange);
            return;
        }

        //reset the defaults if they are out of lower bounds
        if(request.page <= 0) request.page = 1;
        if(request.pageSize <= 0) request.pageSize = 1;
        if(request.path.isEmpty()) request.path = "/";
        

        File root = new File(directory);
        File requestPath;
        if(request.path.startsWith(root.separator)){
            requestPath = new File(directory.concat(request.path));
        }else{
            requestPath = new File(directory.concat(root.separator+request.path));
            request.path = root.separator.concat(request.path);
        }

        //check if we can actually do something with the request
        if(!requestPath.exists()){
            handleErrorReponse(404, String.format("File not found. (%s)",requestPath.getAbsolutePath()),exchange);
            return;
        }

        if(!requestPath.isDirectory() || request.path.endsWith(root.separator.concat("."))||request.path.endsWith(root.separator.concat(".."))){
            handleErrorReponse(400, "Bad Request", exchange);
            return;
        }

        DirectoryResponse response = new DirectoryResponse();

        try {
            
            ArrayList<FileData> files = FileListing.GetDirectoryListing(directory,request.path, (request.page*request.pageSize)-(request.pageSize-1), request.pageSize);

            response.currentPage = request.page;
            response.pageSize = request.pageSize;
            response.currentPath = request.path;
            response.currentTruePath = requestPath.getAbsolutePath().toString();
            response.files = files;

            int fileCount = FileListing.GetDirectoryCount(requestPath);
            response.totalPages = ((fileCount-(fileCount%request.pageSize))/request.pageSize)+(fileCount%request.pageSize==0?0:1);

        } catch (Exception e) {
            handleErrorReponse(500, e.getMessage(), exchange);
            return;
        }
        
        String jsonResponse = gson.toJson(response);

        OutputStream outputStream = exchange.getResponseBody();
        exchange.sendResponseHeaders(200, jsonResponse.length());
        outputStream.write(jsonResponse.getBytes());
        outputStream.flush();
        outputStream.close();

        
    }

    private void handleHealthRequest(HttpExchange exchange) throws IOException{
        
        OutputStream outputStream = exchange.getResponseBody();
        StringBuilder htmlBuilder = new StringBuilder();
        
        htmlBuilder.append("<html>").
                append("<body>").
                append("We are ok.")
                .append("</body>")
                .append("</html>");

        // encode HTML content 
        String htmlResponse = htmlBuilder.toString();

        // this line is a must
        exchange.sendResponseHeaders(200, htmlResponse.length());
        outputStream.write(htmlResponse.getBytes());
        outputStream.flush();
        outputStream.close();

    }

    private void handleErrorReponse(int code, String message, HttpExchange exchange) throws IOException {
        OutputStream outputStream = exchange.getResponseBody();
        
        String response = String.format("{ message:\"%s\" }", message);

        // this line is a must
        exchange.sendResponseHeaders(code, response.length());
        outputStream.write(response.getBytes());
        outputStream.flush();
        outputStream.close();

        AppLogger.info(String.format("%h - %d %s",exchange,code,message));
        
    }
    
}
