package com.entersekt.requests;

public class DirectoryRequest {

    public String path = "/";
    public int page = 1;
    public int pageSize = 10;
    
}
