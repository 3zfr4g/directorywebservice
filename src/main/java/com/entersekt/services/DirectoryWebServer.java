package com.entersekt.services;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import com.entersekt.handlers.DirectoryWebServicesHTTPHandler;
import com.entersekt.helpers.AppLogger;
import com.sun.net.httpserver.*;



public class DirectoryWebServer {

    HttpServer server;
    ThreadPoolExecutor threadPoolExecutor;
    

    public DirectoryWebServer(String dir){
        try {

            threadPoolExecutor = (ThreadPoolExecutor)Executors.newFixedThreadPool(10);
            server = HttpServer.create(new InetSocketAddress("0.0.0.0", 8080), 0);
            server.createContext("/", new  DirectoryWebServicesHTTPHandler(dir));
            server.setExecutor(threadPoolExecutor);    

        } catch (IOException e) {
            AppLogger.severe(e.getLocalizedMessage());
        }
    }

    public void Start(){
        server.start();
        server.getAddress();
        AppLogger.info("Server started on port "+server.getAddress().getPort());
    }

    public void Stop(){
        server.stop(0);
        AppLogger.info("Server stopped.");
    }
    
}
