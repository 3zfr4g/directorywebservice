package com.entersekt.responses;

import java.util.ArrayList;

import com.entersekt.helpers.FileData;





public class DirectoryResponse {
    
    public String currentPath;
    public String currentTruePath;
    public int pageSize;
    public int currentPage;
    public int totalPages;
    public ArrayList<FileData> files;
    
}
