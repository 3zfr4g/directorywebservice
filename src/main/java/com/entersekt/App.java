package com.entersekt;

import com.entersekt.services.*;

public final class App {

    

    private App() {
    }

    /**
     * Says hello to the world.
     * @param args The arguments of the program.
     */
    public static void main(String[] args) {

        DirectoryWebServer server = new DirectoryWebServer("/var/dws");
        server.Start();

        //server.Stop();
    }
}
